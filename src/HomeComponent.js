import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class HomeComponent extends Component {
	constructor(props) {
		super(props);

		this.state = {
			userName: ""
		};
	}

	onUserNameChanged = function(event) {
		let updatedUserName = event.target.value;

		this.setState({
			userName: updatedUserName
		});
	}

	render() {
		let userName = this.state.userName;
		return (
			<div>
				<input value={userName} onChange={this.onUserNameChanged.bind(this)} />
				<Link to={`/user/${userName}`}><button>Test username</button></Link>
			</div>
		);
	}
}

export default HomeComponent;