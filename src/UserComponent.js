import React, { Component } from 'react';
// import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types';

class UserComponent extends Component {
	constructor(props) {
		super(props);

		this.state = {
			username: this.props.match.params.username
		}
	}

	render() {
		return (
			<div>
				Benvenuto { this.state.username }!
			</div>
		);
	}
}

UserComponent.propTypes = {
	username: PropTypes.string.isRequired
};

UserComponent.defaultProps = {
	username: "Default"
};

export default UserComponent;