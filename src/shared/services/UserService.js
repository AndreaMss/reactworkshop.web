import { ajax } from 'rxjs/ajax';
import { map } from 'rxjs/operators'

import settings from '../settings';

class UserService {
	getUsers() {
		return ajax(`${settings.url}/api/users`)
			.pipe(
				map(response => {
					if (response.status && (response.status >= 200 && response.status <= 299))
						return response.response;

					return response;
				})
			);
	}

	getUser(id) {
		return ajax(`${settings.url}/api/users/${id}`)
			.pipe(
				map(response => {
					if (response.status && (response.status >= 200 && response.status <= 299)) {
						// response.response.weightHistory.map(value => {
						// 	value.date = new Date(value.date);
						// 	return value;	
						// });

						return response.response;
					}

					return response;
				})
			);
	}

	editUser(user) {
		let headers = {
			"Content-Type": "application/json"
		};

		return ajax.put(`${settings.url}/api/users/${user.id}`, user, headers);
	}
}

export default new UserService();