import React, { Component } from 'react';
import { withRouter } from 'react-router';

import userService from '../shared/services/UserService';

class UserEditComponent extends Component {
	constructor(props) {
		super(props);

		this.state = {
			user: {},
			formData: {
				name: '',
				surname: '',
				age: ''
			}
		}
	}

	componentDidMount() {
		this.initUser();
	}

	initUser() {
		userService.getUser(this.props.match.params.id).subscribe(
			result => {
				this.setState({
					user: result,
					formData: {
						name: result.name,
						surname: result.surname,
						age: result.age
					}
				});
			},
			error => {
				console.log(error);
			}
		);
	}

	onUserSubmit(event) {
		event.preventDefault();

		let editedUser = { ...this.state.user };
		let formData = this.state.formData;

		editedUser.name = formData.name;
		// editedUser.surname = 

		userService.editUser(editedUser).subscribe(
			(response) => {
				alert("Ho modificato correttamente l'utente.");
				this.props.history.push(`/view/${editedUser.id}`);
			},
			(error) => {
				alert("Errore nella modifica dell'utente!");
			}
		);
	}

	onFormDataChanged(event) {
		let target = event.target;

		let state = {
			formData: {}
		};
		state.formData[target.name] = target.value;

		this.setState(state);
	}

	render() {
		console.log({ ...this.state.user });
		return (
			<div>
				<form onSubmit={this.onUserSubmit.bind(this)}>
					<label>
						Name:
						<input type="text" name="name" value={this.state.formData.name}
							onChange={this.onFormDataChanged.bind(this)} />
					</label>
					<input type="submit" value="Submit" />
				</form>
			</div>
		);
	}
}

UserEditComponent.propTypes = {

};

export default withRouter(UserEditComponent);