import React, { Component } from 'react';
import userService from '../shared/services/UserService';
import UserWeightHistoryComponent from './UserWeightHistoryComponent';

import './UserViewComponent.css';

class UserViewComponent extends Component {
	constructor(props) {
		super(props);

		this.state = {
			user: {}
		}
	}

	componentDidMount() {
		this.initUser();
	}

	initUser() {
		userService.getUser(this.props.match.params.id).subscribe(
			result => {
				this.setState({ user: result });
			},
			error => {
				console.log(error);
			}
		);
	}

	renderUser(user) {
		return (
			<div>
				<h1>{user.name} {user.surname}, di {user.age} anni</h1>
				<div className="weightHistoryContainer">
					<UserWeightHistoryComponent weightHistory={user.weightHistory} />
				</div>
			</div>
		);
	}

	render() {
		let user = this.state.user;

		return (
			<div>
				{user.id ? this.renderUser(user) : ''}
			</div>
		);
	}
}

UserViewComponent.propTypes = {
};

export default UserViewComponent;