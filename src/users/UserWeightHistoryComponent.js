import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ResponsiveContainer, LineChart, XAxis, YAxis, CartesianGrid, Line } from 'recharts'

class UserWeightHistoryComponent extends Component {
	formatDate(date) {
		return new Date(date).toLocaleDateString();
	}

	formatWeight(weight) {
		return weight = Math.round(weight * 100) / 100;
	}

	render() {
		let weightHistory = this.props.weightHistory;
		let minWeight = Math.min.apply(null, weightHistory.map(w => w.weight));
		let maxWeight = Math.max.apply(null, weightHistory.map(w => w.weight));
		let yAxisMargin = (maxWeight - minWeight) * 0.1;
		let yAxisDomainMin = minWeight - yAxisMargin;
		let yAxisDomainMax = maxWeight + yAxisMargin;

		return (
			<ResponsiveContainer width="100%" height="100%">
				<LineChart data={weightHistory}>
					<XAxis dataKey="date" label={{ value: "Date", position: "insideBottom" }} tickFormatter={this.formatDate} />
					<YAxis domain={[ yAxisDomainMin, yAxisDomainMax ]} label={{ value: "Weight (Kg)", angle: -90, position: "insideLeft" }} tickFormatter={this.formatWeight} />
					<CartesianGrid stroke="black" strokeDasharray="5 5" />
					<Line type="monotone" dataKey="weight" stroke="#be3e2b" />
				</LineChart>
			</ResponsiveContainer>
		);
	}
}

UserWeightHistoryComponent.propTypes = {
	weightHistory: PropTypes.arrayOf(PropTypes.shape({
		weight: PropTypes.number.isRequired,
		date: PropTypes.string.isRequired
	})).isRequired
};

export default UserWeightHistoryComponent;