import React, { Component } from 'react';

import userService from '../shared/services/UserService';
import UserCardComponent from './UserCardComponent';

class UsersComponent extends Component {
	constructor(props) {
		super(props);

		this.state = {
			users: []
		}
	}

	componentDidMount() {
		this.initUsers();
	}

	initUsers() {
		userService.getUsers().subscribe(
			result => {
				this.setState({ users: result });
			},
			error => {
				console.log(error);
			}
		);
	}

	renderCards = (users) =>
		<ul>
			{
				users.map(user =>
					<UserCardComponent
						key={user.id}
						userId={user.id}
						userName={user.name}
						userSurname={user.surname}
						userAge={user.age} />
				)
			}
		</ul>

	render() {
		return this.renderCards(this.state.users);
	}
}

UsersComponent.propTypes = {

};

export default UsersComponent;