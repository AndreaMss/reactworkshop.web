import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import './UserCardComponent.css'


class UserCardComponent extends Component {
	renderUserInfo() {
		return `${this.props.userName} ${this.props.userSurname} di ${this.props.userAge} anni `;
	}

	render() {
		return (
			<li>
				{this.renderUserInfo()}
				<Link to={`/view/${this.props.userId}`}><button className="userBtn viewBtn">Visualizza</button></Link>
				<Link to={`/edit/${this.props.userId}`}><button className="userBtn editBtn">Modifica</button></Link>
			</li>
		);
	}
}

UserCardComponent.propTypes = {
	userId: PropTypes.number.isRequired,
	userName: PropTypes.string.isRequired,
	userSurname: PropTypes.string.isRequired,
	userAge: PropTypes.number.isRequired
};

export default UserCardComponent;