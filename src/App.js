import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import AppRouting from './AppRouting';
import HeaderComponent from './HeaderComponent';

import './App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <HeaderComponent />
          <div className="content">
            <AppRouting />
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
