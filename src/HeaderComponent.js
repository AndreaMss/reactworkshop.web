import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './HeaderComponent.css';

class HeaderComponent extends Component {
	render() {
		return (
			<nav className="header">
				<Link className="headerField" to="/"><b>Home</b></Link>
				<Link className="headerField" to="/view"><b>Utenti</b></Link>
			</nav>
		);
	}
}

export default HeaderComponent;