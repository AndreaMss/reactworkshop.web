import React, { Component } from 'react';
import { Switch, Route } from 'react-router';

import UserComponent from './UserComponent';
import NotFoundComponent from './NotFoundComponent';
import UsersComponent from './users/UsersComponent';
import UserViewComponent from './users/UserViewComponent';
import HomeComponent from './HomeComponent';
import UserEditComponent from './users/UserEditComponent';

class AppRouting extends Component {
	render() {
		return (
			<Switch>
				<Route
					path="/"
					exact
					component={HomeComponent}
				/>

				<Route
					path="/user/:username"
					exact
					component={UserComponent}
				/>

				<Route
					path="/view"
					exact
					component={UsersComponent}
				/>

				<Route
					path="/view/:id"
					exact
					component={UserViewComponent}
				/>

				<Route
					path="/edit/:id"
					exact
					component={UserEditComponent}
				/>

				<Route
					render={NotFoundComponent}
				/>
			</Switch>
		);
	}
}

AppRouting.propTypes = {

};

export default AppRouting;